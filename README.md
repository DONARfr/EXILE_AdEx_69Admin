# 69WASTED Admin APP for Exile ExAd

## Installation  
1. Copy all files in your Mission  
2. Add following to your Config.cpp
```
class ExAd_69Admin 
	{
		title = "69 Admin";
		controlID = 50700;
		config = "ExadClient\XM8\Apps\69admin\config.sqf";
		logo = "ExadClient\XM8\Apps\69admin\Icon_69admin.paa";
		onLoad = "ExAdClient\XM8\Apps\69admin\onLoad.sqf";
		onOpen = "ExAdClient\XM8\Apps\69admin\onOpen.sqf";
		onClose = "ExAdClient\XM8\Apps\69admin\onClose.sqf";
	};
```
3. Add ```ExAd_69Admin``` to ```extraApps[]```

## Update Instructions
Replace changed files 

## Dependencies
* [Exile](http://www.exilemod.com/downloads/)
* [AdEx Core](https://github.com/Bjanski/ExAd) 



## Contribute
If you find a bug or have some suggestions for improvement  open a new issue [here](https://gitlab.com/DONARfr/EXILE_AdEx_69Admin/issues) 

## License
Apache License 2.0

